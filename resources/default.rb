resource_name :dotfiles

property :repository, String, name_attribute: true
property :revision, String, default: "master"
property :user, String
property :group, String
property :home_path, [String, Proc]
# if force_install is set to true, changes to local copies
# of dotfiles will be overwritten
property :force_install, [TrueClass, FalseClass], default: false

actions :install
default_action :install

action :install do
  home_path =
    if new_resource.home_path === Proc
      new_resource.home_path.call
    else
      new_resource.home_path
    end

  install_path = ::File.join(home_path, ".dotfiles")

  command = "git clone --bare #{new_resource.repository} #{install_path}"

  force_install = new_resource.force_install

  bash "run a bare checkout of #{new_resource.repository} to #{install_path}" do
    code command
    user        new_resource.user
    group       new_resource.group
    not_if { ::File.directory?(install_path) }
    notifies :run, "ruby_block[force install]", :immediately
    notifies :run, "bash[unset bare]", :immediately
    notifies :run, "bash[set HOME as working directory]", :immediately
    notifies :run, "bash[ignore untracked files]", :immediately
  end

  ruby_block "force install" do
    action :nothing
    block { force_install = true }
  end

  bash "unset bare" do
    action :nothing
    code "git config --local core.bare false"
    cwd install_path
    user        new_resource.user
    group       new_resource.group
  end

  bash "set HOME as working directory" do
    action :nothing
    code "git config --local core.worktree #{home_path}"
    cwd install_path
    user        new_resource.user
    group       new_resource.group
  end

  bash "ignore untracked files" do
    action :nothing
    code "git config --local status.showUntrackedFiles no"
    cwd install_path
    user        new_resource.user
    group       new_resource.group
  end

  bash "install dotfiles" do
    code "git reset --hard HEAD --"
    cwd install_path
    user        new_resource.user
    group       new_resource.group
    only_if { force_install }
  end
end
