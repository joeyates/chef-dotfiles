name             "dotfiles"
maintainer       "Joe Yates"
maintainer_email "joe.g.yates@gmail.com"
license          "MIT"
description      "Installs a user's dotfiles repo"
long_description IO.read(File.expand_path("README.md", __dir__))
version          "0.1.0"
