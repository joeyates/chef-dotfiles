# dotfiles cookbook

Installs and maintain dotfiles in a Git repo.

This library assumes you have your personal dotfiles stored in a Git repo
somewhere online.

# Usage

```ruby
dotfiles "git@example.com:my/dotfiles" do
  user        "me"
  group       "me"
  home_path   "/home/me"
end
```

By the way, when first run you'll need a `known_hosts` entry for
the Git server where you store you dotfiles. Without the entry, the Git clone
will not work.

You can pre-install your server in your `known_hosts` with the `shell`
cookbook's `shell_known_hosts_entry` resource.

# Your Dotfiles Repo

This approach allows you to manage your dotfiles simply:

* No symlinks,
* No scripts,
* Just Git configuration.

It is inspired by this comment on Hacker News:
https://news.ycombinator.com/item?id=11070797

# How it works

Your dotfiles repo is cloned to ~/.dotfiles, but Git is told that
the working directory is your home directory.

You can install your dotfiles, add new files,
check if you've made local changes and apply them to the repo.

# Manual Setup

If you want to do the same thing manually, you can do the following:

```shell
$ git clone --bare <my repo> ~/.dotfiles
$ cd ~/.dotfiles
$ git config --local core.bare false
$ git config --local core.worktree <my home>
$ git config --local status.showUntrackedFiles no
$ git reset
```

Now, run the following to check what will change when you "install" your
dotfiles:

```
$ git status
```

Beware - the next line installs the current version of files in your dotfiles
repo, potentially overwriting different versions in your home directory (or
subdirectories).

```
$ git reset --hard HEAD --
```

# Maintenance

`cd` to your `.dotfiles` directory.

You can manage the repo as usual.

Run `git status` to see what has changed.

Note that all file operations have a `..` prefix:

```shell
$ git add ../foobar
```

# Outside the .dotfiles directory

Use the following to manage your repo without having to `cd` to `.dotfiles`:

```shell
git --git-dir=$HOME/.dotfiles --work-tree=$HOME <command> [ARGS]
```
